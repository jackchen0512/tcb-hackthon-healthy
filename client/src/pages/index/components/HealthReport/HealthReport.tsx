import { Button, Text, View } from '@tarojs/components'
import Taro from '@tarojs/taro'
import './style.less'

export function HealthReport() {
  const now = new Date()
  const date = `${now.getFullYear()}-${now.getMonth() + 1}-${now.getDate()}`

  const toReport = () => {
    Taro.navigateToMiniProgram({
      appId: 'wx34b0738d0eef5f78',
      path: 'pages/forms/publish?token=zYhJ5Y',
    })
  }

  return (
    <View className="container">
      <Button onClick={toReport} className="report-button">
        <View className="report-button-icon" />
        <Text>今日健康上报</Text>
      </Button>
      <Text className="date">{date}</Text>
      <View className="text-container">
        <Text className="title">健康状态：</Text>
        <Text className="tip">记得上报健康情况哦</Text>
      </View>
      <View className="text-container">
        <Text className="title">行程排查：</Text>
        <Text className="tip">填写行程，后台将查询是否曾与确诊病患同行。有情况会第一时间通知并采取措施</Text>
      </View>
    </View>
  )
}
